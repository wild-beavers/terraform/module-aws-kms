output "precomputed" {
  value = {
    aws_iam_policies = local.iam_should_create_external_policy || local.iam_export_json ? { for k, v in var.iam_policy_entity_arns :
      k => merge(
        local.iam_should_create_external_policy ? {
          arn = provider::aws::arn_build("aws", "iam", "", local.current_account_id, "policy${var.iam_policy_path}${var.prefix}${format(var.iam_policy_name_template, k)}")
        } : null,
        local.iam_export_json ? { json = data.aws_iam_policy_document.external_kms[k].json } : null
      )
    } : null
    aws_kms_aliases = { for k, v in var.kms_keys :
      k => {
        arns = concat(
          local.kms_should_create ? [
            provider::aws::arn_build("aws", "kms", local.current_region, local.current_account_id, "alias/${var.prefix}${v.alias}")
          ] : [],
          local.kms_should_create_kms_replica ? [
            provider::aws::arn_build("aws", "kms", local.replica_region, local.current_account_id, "alias/${var.prefix}${v.alias}")
          ] : [],
        )
      }
    }
    aws_iam_policy_actions = var.iam_policy_export_actions ? local.iam_external_actions : null
  }
}
