## 2.2.0

- feat: allows "delete" (delete only) scope globally

## 2.1.5

- maintenance: migrate module to Terraform registry
- chore: update pre-commit dependencies

## 2.1.4

- fix: wrong pre-computed external IAM policy KMS key ARN

## 2.1.3

- fix: validates entities through provider function, for better edge case validations

## 2.1.2

- fix: (iam) internal policy was always created

## 2.1.1

- fix: (iam) error was raised to ask for template and description when only exporting jsons
- fix: (iam) policies json not outputted when only exporting JSON

## 2.1.0

- feat: adds optional `var.prefix` to prefix all names
- fix: error was not correctly raised when "full" role is not given

## 2.0.0

- feat: (BREAKING) changes outputs:
   - `kms_key.xxx.(id|alias|key_id|multi_region)` => `aws_kms_keys.xxx.(id|alias|key_id|multi_region)`
   - `kms_key.xxx.arns` => *removed*
   - `kms_key.xxx.alias_(id|arn)` => `aws_kms_aliases.xxx.(id|arn)`
   - `kms_key.xxx.alias_arns` => *removed*
   - `kms_key.xxx.multi_region_replica_yyy` => `replica_aws_kms_keys.xxx.yyy`
   - `kms_key.xxx.multi_region_replica_alias_yyy` => `replica_aws_kms_aliases.xxx.yyy`
   - `kms_key_iam_policy.xxx` => `aws_iam_policies.xxx`
   - `kms_key_iam_policy_jsons` => `precomputed.aws_iam_policy_jsons`
   - `kms_key_iam_actions` => `precomputed.aws_iam_policy_actions`
- feat: adds non-mandatory variables: `var.current_region`, `var.replica_region`, `var.current_account_id`
- feat: adds the ability to attach grants through `var.kms_grants`

## 1.2.0

- feat: adds `var.iam_policy_restrict_by_regions` to allow KMS restriction by region.
- fix: allow wildcard region in `var.iam_policy_source_arns` and `var.iam_policy_entity_arns` validation
- fix: allow service if the account ID match `var.iam_policy_restrict_by_account_ids`
- fix: removes `deployer_name` in examples

## 1.1.0

- feat: handles `var.iam_policy_source_arns` to pass allowed sourceARNs

## 1.0.1

- fix: makes sure passing a null element inside `var.iam_policy_entity_arns` does not break the module

## 1.0.0

- feat: (BREAKING) `var.iam_policy_entity_arns` is now a `map(map(string)))`, instead of a `map(list(string))`
- feat: (BREAKING) removes `var.iam_admin_arns`, now use: `var.iam_policy_entity_arns.full`
- feat: adds auditor scope: `var.iam_policy_entity_arns.audit` to allow entities to manage KMS without accessing the data
- feat: adds `var.iam_policy_export_actions` to export computed actions for KMS in `kms_key_iam_actions`
- refactor: simplifies code by removing `coalesce` when variables could be `nullable = false` instead
- doc: updates LICENSE
- chore: bump pre-commit hooks

## 0.1.0

- feat: adds `kms_key.arns` and `kms_key.alias_arns` outputs containing ARN and replica ARNs if exists

## 0.0.0

- tech: initial code
