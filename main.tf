####
# Variables
####

variable "prefix" {
  description = "Prefix to be used for all resources names. Specifically useful for tests. A 4-characters alphanumeric string. It must start by a lowercase letter."
  type        = string
  default     = ""
  nullable    = false

  validation {
    condition     = var.prefix == "" || can(regex("^[a-z][a-zA-Z0-9]{3}$", var.prefix))
    error_message = "“var.prefix” does not match “^[a-z][a-zA-Z0-9]{3}$”."
  }
}

variable "tags" {
  description = "Tags to be shared with all resources of this module."
  type        = map(string)
  default     = {}
  nullable    = false
}

variable "kms_keys" {
  description = <<-DOCUMENTATION
Options of the KMS key to create.
If given and and `var.replica_enabled`, this key will be use also for the replica, unless `var.replica_kms_key_id` is set.
If given, `var.kms_key_id` will be ignored.
Keys are free values.

  - alias                   (required, string):                      KMS key alias; Display name of the KMS key. Omit the `alias/`.
  - principal_actions       (optional, map(list(string))):           List of read-only, read-write, read-write-delete, audit and full allowed IAM actions for the principals defined by "var.iam_policy_entity_arns[key]" variable, or nobody, if variable value is undefined. The four expected keys are "ro", "rw", "rwd", "delete", "audit" and "full". If this is not provided, actions default to corresponding `var.iam_policy_external_actions_...`.
  - policy_json             (optional, string):                      Additional policy to attach to the KMS key, merged with a baseline internal policy. Make sure to provide "var.iam_admin_arns" to avoid getting blocked.
  - service_principal       (optional, string):                      The name of the service authorized to access the key.
  - service_actions         (optional, list(string)):                List of allowed IAM actions for the "service_principal" authorized to access the key.
  - description             (optional, string, ""):                  Description of the key.
  - usage                   (optional, string, "ENCRYPT_DECRYPT"):   Specifies the intended use of the key. Valid values: `ENCRYPT_DECRYPT`, `SIGN_VERIFY`, or `GENERATE_VERIFY_MAC`.
  - spec                    (optional, string, "SYMMETRIC_DEFAULT"): Specifies whether the key contains a symmetric key or an asymmetric key pair and the encryption algorithms or signing algorithms that the key supports. Valid values: `SYMMETRIC_DEFAULT`, `RSA_2048`, `RSA_3072`, `RSA_4096`, `HMAC_256`, `ECC_NIST_P256`, `ECC_NIST_P384`, `ECC_NIST_P521`, or `ECC_SECG_P256K1`.
  - rotation_enabled        (optional, bool, true):                  Whether to automatically rotate the KMS key linked to the secrets.
  - deletion_window_in_days (optional, number, 7):                   The waiting period, specified in number of days. After the waiting period ends, AWS KMS deletes the KMS key. If you specify a value, it must be between `7` and `30`, inclusive. If you do not specify a value, it defaults to `7`.
  - tags                    (optional, map(string)):                 Tags to be used by the KMS key of this module.
DOCUMENTATION
  type = map(object({
    alias                   = string
    principal_actions       = optional(map(list(string)))
    policy_json             = optional(string, null)
    service_principal       = optional(string, null)
    service_actions         = optional(list(string), null)
    description             = optional(string, "")
    usage                   = optional(string, "ENCRYPT_DECRYPT")
    spec                    = optional(string, "SYMMETRIC_DEFAULT")
    rotation_enabled        = optional(bool, true)
    deletion_window_in_days = optional(number, 7)
    tags                    = optional(map(string), {})
  }))
  default  = {}
  nullable = false

  validation {
    condition = var.kms_keys == null || (!contains([for key, kms_key in var.kms_keys : (
      (try(kms_key.alias, null) == null || can(regex("[A-Za-z\\-_\\/]{1,256}", try(kms_key.alias, null)))) &&
      (kms_key.policy_json == null || try(jsondecode(kms_key.policy_json), null) != null) &&
      (1 <= length(coalesce(try(kms_key.description, null), "empty")) && length(coalesce(try(kms_key.description, null), "empty")) <= 512) &&
      (contains(["ENCRYPT_DECRYPT", "SIGN_VERIFY", "GENERATE_VERIFY_MAC"], coalesce(kms_key.usage, "ENCRYPT_DECRYPT"))) &&
      (contains(["SYMMETRIC_DEFAULT", "RSA_2048", "RSA_3072", "RSA_4096", "HMAC_256", "ECC_NIST_P256", "ECC_NIST_P384", "ECC_NIST_P521", "ECC_SECG_P256K1"], coalesce(kms_key.spec, "ENCRYPT_DECRYPT"))) &&
      (!contains([for principal_action_key, principal_action in coalesce(kms_key.principal_actions, {}) :
        contains(["ro", "rw", "rwd", "delete", "full", "audit"], principal_action_key)
      ], false)) &&
      (try(kms_key.deletion_window_in_days, null) == null || 7 <= coalesce(try(kms_key.deletion_window_in_days, null), 7) && coalesce(try(kms_key.deletion_window_in_days, null), 7) <= 30)
    )], false))
    error_message = "One or more of “var.kms_keys” are invalid."
  }
}

variable "kms_grants" {
  description = <<-DOCUMENTATION
Grants to create.
Keys must correspond to `var.kms_keys` keys.
Second level keys are free values.

  - name                  (required, string):       A friendly name for identifying the grant.
  - grantee_principal     (required, string):       The principal that is given permission to perform the operations that the grant permits in ARN format. Note that due to eventual consistency issues around IAM principals, terraform's state may not always be refreshed to reflect what is true in AWS.
  - operations            (required, list(string)): A list of operations that the grant permits. The permitted values are: `Decrypt`, `Encrypt`, `GenerateDataKey`, `GenerateDataKeyWithoutPlaintext`, `ReEncryptFrom`, `ReEncryptTo`, `Sign`, `Verify`, `GetPublicKey`, `CreateGrant`, `RetireGrant`, `DescribeKey`, `GenerateDataKeyPair`, or `GenerateDataKeyPairWithoutPlaintext`.
  - retiring_principal    (optional, string):       The principal that is given permission to retire the grant by using RetireGrant operation in ARN format. Note that due to eventual consistency issues around IAM principals, terraform's state may not always be refreshed to reflect what is true in AWS.
  - grant_creation_tokens (optional, list(string)): A list of grant tokens to be used when creating the grant. See Grant Tokens for more information about grant tokens.
  - retire_on_delete      (optional, bool):         If set to false (the default) the grants will be revoked upon deletion, and if set to true the grants will try to be retired upon deletion. Note that retiring grants requires special permissions, hence why we default to revoking grants. See RetireGrant for more information.
DOCUMENTATION
  type = map(map(object({
    name                  = string
    grantee_principal     = string
    operations            = list(string)
    retiring_principal    = optional(string)
    grant_creation_tokens = optional(list(string))
    retire_on_delete      = optional(bool, false)
  })))
  default  = {}
  nullable = false

  validation {
    condition     = length(var.kms_grants) == 0 || length(setintersection(keys(var.kms_keys), keys(var.kms_grants))) == length(keys(var.kms_grants))
    error_message = "“var.kms_grants” must have the corresponding keys with “var.kms_keys”."
  }
  validation {
    condition = alltrue(flatten([for scope, grants in var.kms_grants :
      [for grant in grants : (
        provider::aws::arn_parse(grant.grantee_principal).service == "iam" &&
        (grant.retiring_principal == null || try(provider::aws::arn_parse(grant.retiring_principal).service == "iam", false)) &&
        (length(setintersection(["Decrypt", "Encrypt", "GenerateDataKey", "GenerateDataKeyWithoutPlaintext", "ReEncryptFrom", "ReEncryptTo", "Sign", "Verify", "GetPublicKey", "CreateGrant", "RetireGrant", "DescribeKey", "GenerateDataKeyPair", "GenerateDataKeyPairWithoutPlaintext"], grant.operations)) == length(grant.operations))
      )]
    ]))
    error_message = "One or more of “var.kms_grants” are invalid."
  }
}

variable "replica_enabled" {
  description = "Whether to replicate the KMS keys into the `aws.replica` region."
  type        = bool
  default     = false
  nullable    = false
}

variable "iam_policy_entity_arns" {
  description = <<-DOCUMENTATION
Restrict access the given IAM entities (roles or users).
Allowed keys (scopes) are `ro`, `rw`, `rwd`, `audit` or `full`, each specifying the expected level of access for entities within them.
Wildcards are allowed.
DOCUMENTATION
  type        = map(map(string))
  default     = {}
  nullable    = false

  validation {
    condition = (!contains([
      for key, arns in var.iam_policy_entity_arns :
      (
        contains(["ro", "rw", "rwd", "delete", "audit", "full"], key) &&
        (
          alltrue([for arn in values(arns) :
            (arn == null || contains(["iam", "sts"], try(provider::aws::arn_parse(arn).service, "no")))
          ])
        )
      )
    ], false))
    error_message = "One or more “var.iam_policy_entity_arns” are invalid."
  }
}

variable "iam_policy_source_arns" {
  description = <<-DOCUMENTATION
Restrict access to the key to the given ARN sources.
Allowed keys (scopes) are `ro`, `rw`, `rwd`, `audit` or `full`, each specifying the expected level of access for entities within them.
Wildcards are allowed.
DOCUMENTATION
  type        = map(map(string))
  default     = {}
  nullable    = false

  validation {
    condition = ((!contains([
      for key, arns in var.iam_policy_source_arns :
      (
        contains(["ro", "rw", "rwd", "delete", "audit", "full"], key) &&
        (
          alltrue([for arn in values(arns) :
            (arn == null || try(provider::aws::arn_parse(arn).service, "no") != "no")
          ])
        )
      )
    ], false)))
    error_message = "One or more “var.iam_policy_source_arns” are invalid."
  }
}

variable "iam_policy_sid_prefix" {
  description = "Use a prefix for all `Sid` of all the policies - internal and external - created by this module."
  type        = string
  default     = ""
  nullable    = false
}

variable "iam_policy_restrict_by_account_ids" {
  description = "Restrict resources created by this module by the given account IDs. Internal policies will whitelist given list. If `null`, no restriction will be applied. Especially useful when IAM entities contains accounts wildcards."
  type        = list(string)
  default     = []
  nullable    = false

  validation {
    condition = ((!contains([
      for id in var.iam_policy_restrict_by_account_ids :
      (
        (can(regex("^[0-9]{12}$", id)))
      )
    ], false)))
    error_message = "One or more “var.iam_policy_restrict_by_account_ids” are invalid."
  }
}

variable "iam_policy_restrict_by_regions" {
  description = "Restrict resources created by this module by the given regions. Internal policies will whitelist given list. If `null`, no restriction will be applied. Especially useful when IAM entities contains region wildcards."
  type        = list(string)
  default     = []
  nullable    = false

  validation {
    condition = ((!contains([
      for id in var.iam_policy_restrict_by_regions :
      (
        (can(regex("^[a-z]{2}-[a-z]{4,10}-[1-9]{1}$", id)))
      )
    ], false)))
    error_message = "One or more “var.iam_policy_restrict_by_regions” are invalid."
  }
}

####
# Locals
####

locals {
  tags = merge(
    var.tags,
    {
      origin     = "gitlab.com/wild-beavers/terraform/module-aws-kms"
      managed-by = "terraform"
    },
  )

  kms_should_create             = var.kms_keys != null
  kms_should_create_internal    = local.kms_should_create && (length(var.iam_policy_entity_arns) > 0 || length(var.iam_policy_source_arns) > 0)
  kms_should_create_kms_replica = var.replica_enabled && local.kms_should_create

  kms_iam_policy_prefix = try(chomp(var.iam_policy_sid_prefix), "")

  kms_keys_actions_and_entities_and_actions = { for key, kms_key in var.kms_keys :
    key => { for entity_key, entity_arns in var.iam_policy_entity_arns :
      entity_key => {
        entity_arns = values(entity_arns)
        actions     = kms_key.principal_actions != null ? lookup(kms_key.principal_actions, entity_key, local.iam_external_actions[entity_key]) : local.iam_external_actions[entity_key]
      }
    }
  }
  kms_keys_actions_and_sources_and_actions = { for key, kms_key in var.kms_keys :
    key => { for entity_key, entity_arns in var.iam_policy_source_arns :
      entity_key => {
        entity_arns = values(entity_arns)
        actions     = kms_key.principal_actions != null ? lookup(kms_key.principal_actions, entity_key, local.iam_external_actions[entity_key]) : local.iam_external_actions[entity_key]
      }
    }
  }
}

####
# Data
####

data "aws_iam_policy_document" "internal_kms" {
  for_each = local.kms_should_create_internal ? var.kms_keys : {}

  source_policy_documents = each.value.policy_json != null ? [each.value.policy_json] : null

  dynamic "statement" {
    for_each = each.value.service_principal != null ? { (each.value.service_principal) : each.value.service_actions } : {}

    content {
      sid       = "${local.kms_iam_policy_prefix}Service"
      actions   = statement.value
      resources = ["*"]
      principals {
        identifiers = [format("%s.amazonaws.com", statement.key)]
        type        = "Service"
      }
    }
  }

  dynamic "statement" {
    for_each = local.kms_keys_actions_and_sources_and_actions[each.key]

    content {
      sid       = "${local.kms_iam_policy_prefix}Source${upper(statement.key)}"
      actions   = statement.value.actions
      resources = ["*"]
      principals {
        type        = "AWS"
        identifiers = ["*"]
      }
      condition {
        test     = "StringLike"
        values   = sort(compact(statement.value.entity_arns))
        variable = "aws:sourceArn"
      }
    }
  }

  dynamic "statement" {
    for_each = local.kms_keys_actions_and_entities_and_actions[each.key]

    content {
      sid       = "${local.kms_iam_policy_prefix}Principals${upper(statement.key)}"
      actions   = statement.value.actions
      resources = ["*"]
      principals {
        type        = "AWS"
        identifiers = ["*"]
      }
      condition {
        test     = "StringLike"
        values   = sort(compact(statement.value.entity_arns))
        variable = "aws:principalArn"
      }
    }
  }

  dynamic "statement" {
    for_each = length(var.iam_policy_restrict_by_account_ids) > 0 ? [1] : []

    content {
      sid     = "${local.kms_iam_policy_prefix}AccountsRestriction"
      effect  = "Deny"
      actions = ["kms:*"]
      principals {
        type        = "AWS"
        identifiers = ["*"]
      }
      condition {
        test     = "StringNotEqualsIfExists"
        values   = sort(compact(var.iam_policy_restrict_by_account_ids))
        variable = "aws:PrincipalAccount"
      }
      condition {
        test     = "StringNotEqualsIfExists"
        values   = sort(compact(var.iam_policy_restrict_by_account_ids))
        variable = "aws:SourceAccount"
      }
    }
  }

  dynamic "statement" {
    for_each = length(var.iam_policy_restrict_by_regions) > 0 ? [1] : []

    content {
      sid     = "${local.kms_iam_policy_prefix}RegionsRestriction"
      effect  = "Deny"
      actions = ["kms:*"]
      principals {
        type        = "AWS"
        identifiers = ["*"]
      }
      condition {
        test     = "StringNotEquals"
        values   = sort(compact(var.iam_policy_restrict_by_regions))
        variable = "aws:RequestedRegion"
      }
    }
  }

  lifecycle {
    precondition {
      condition     = (length(var.iam_policy_entity_arns) == 0 && length(var.iam_policy_source_arns) == 0) || length(lookup(var.iam_policy_entity_arns, "full", {})) > 0
      error_message = "`var.iam_policy_entity_arns.full` must contain at least one element, to prevent the KMS key to become unmanageable."
    }
  }
}

####
# Resources
####

resource "aws_kms_key" "this" {
  for_each = local.kms_should_create ? var.kms_keys : {}

  description              = each.value.description
  key_usage                = each.value.usage
  customer_master_key_spec = each.value.spec
  enable_key_rotation      = each.value.rotation_enabled
  deletion_window_in_days  = each.value.deletion_window_in_days
  multi_region             = var.replica_enabled

  tags = merge(
    {
      Name        = "${var.prefix}${each.value.alias}"
      Description = each.value.description
    },
    local.tags,
    each.value.tags,
  )
}

resource "aws_kms_alias" "this" {
  for_each = local.kms_should_create ? var.kms_keys : {}

  name          = "alias/${var.prefix}${each.value.alias}"
  target_key_id = aws_kms_key.this[each.key].key_id
}

resource "aws_kms_key_policy" "this" {
  for_each = local.kms_should_create_internal ? var.kms_keys : {}

  key_id = aws_kms_key.this[each.key].key_id
  policy = data.aws_iam_policy_document.internal_kms[each.key].json

  depends_on = [
    aws_kms_alias.this
  ]
}

resource "aws_kms_replica_key" "replica" {
  for_each = local.kms_should_create_kms_replica ? var.kms_keys : {}

  primary_key_arn = aws_kms_key.this[each.key].arn

  description             = each.value.description
  deletion_window_in_days = each.value.deletion_window_in_days

  tags = merge(
    {
      Name        = "${var.prefix}${each.value.alias}"
      Description = each.value.description
    },
    local.tags,
    each.value.tags,
  )

  provider = aws.replica
}

resource "aws_kms_alias" "replica" {
  for_each = local.kms_should_create_kms_replica ? var.kms_keys : {}

  name          = "alias/${var.prefix}${each.value.alias}"
  target_key_id = aws_kms_replica_key.replica[each.key].key_id

  provider = aws.replica
}

resource "aws_kms_key_policy" "replica" {
  for_each = local.kms_should_create_kms_replica && local.kms_should_create_internal ? var.kms_keys : {}

  key_id = aws_kms_replica_key.replica[each.key].id
  policy = data.aws_iam_policy_document.internal_kms[each.key].json

  provider = aws.replica

  depends_on = [
    aws_kms_key_policy.this,
    aws_kms_alias.replica
  ]
}

resource "aws_kms_grant" "this" {
  for_each = merge([for k, grants in var.kms_grants :
    { for grant_key, grant in grants :
      "${k}_${grant_key}" => merge(grant, { key = k })
  }]...)

  name                  = "${var.prefix}${each.value.name}"
  key_id                = aws_kms_key.this[each.value.key].id
  grantee_principal     = each.value.grantee_principal
  operations            = each.value.operations
  retiring_principal    = each.value.retiring_principal
  grant_creation_tokens = each.value.grant_creation_tokens
  retire_on_delete      = each.value.retire_on_delete
}

resource "aws_kms_grant" "replica" {
  for_each = local.kms_should_create_kms_replica ? merge([for k, grants in var.kms_grants :
    { for grant_key, grant in grants :
      "${k}_${grant_key}" => merge(grant, { key = k })
  }]...) : {}

  name                  = "${var.prefix}${each.value.name}"
  key_id                = aws_kms_replica_key.replica[each.value.key].id
  grantee_principal     = each.value.grantee_principal
  operations            = each.value.operations
  retiring_principal    = each.value.retiring_principal
  grant_creation_tokens = each.value.grant_creation_tokens
  retire_on_delete      = each.value.retire_on_delete

  provider = aws.replica
}

####
# Outputs
####

output "aws_kms_keys" {
  value = local.kms_should_create ? { for k, v in aws_kms_key.this :
    k => { for i, j in v : i => j if !contains(["custom_key_store_id", "policy", "tags", "is_enabled", "enabled"], i) }
  } : null
}

output "aws_kms_aliases" {
  value = local.kms_should_create ? { for k, v in aws_kms_alias.this :
    k => { for i, j in v : i => j if !contains(["name_prefix"], i) }
  } : null
}

output "aws_kms_grants" {
  value = length(var.kms_grants) > 0 ? { for k, v in aws_kms_grant.this :
    k => { for i, j in v : i => j if !contains(["grant_token", "grant_creation_tokens", "operations"], i) }
  } : null
}

output "replica_aws_kms_keys" {
  value = local.kms_should_create_kms_replica ? { for k, v in aws_kms_replica_key.replica :
    k => { for i, j in v : i => j if !contains(["custom_key_store_id", "policy", "tags", "is_enabled", "enabled"], i) }
  } : null
}

output "replica_aws_kms_aliases" {
  value = local.kms_should_create_kms_replica ? { for k, v in aws_kms_alias.replica :
    k => { for i, j in v : i => j if !contains(["name_prefix"], i) }
  } : null
}

output "replica_aws_kms_grants" {
  value = length(var.kms_grants) > 0 ? { for k, v in aws_kms_grant.replica :
    k => { for i, j in v : i => j if !contains(["grant_token", "grant_creation_tokens", "operations"], i) }
  } : null
}
