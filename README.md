# terraform-module-kms

Terraform module to create and manage KMS and related resources.

## Limitations

Feel free to make a pull request to circumvent these limitations.

- Each key can only be consumed by a single AWS service (e.g. `ec2`, `s3`, etc.).
- Internal IAM policy will grant *maximal* permissions for provided IAM entities, while `policy_json` can be used to further restrict KMS key accesses.
- Does not yet manage custom key stores (`aws_kms_custom_key_store`)
- Does not yet manage grants (`aws_kms_grant`)
- Does not yet manage external keys (`aws_kms_external_key`)

<!-- BEGIN_TF_DOCS -->
## Requirements

| Name | Version |
|------|---------|
| terraform | >= 1.9 |
| aws | >= 5.9 |

## Providers

| Name | Version |
|------|---------|
| aws | >= 5.9 |
| aws.replica | >= 5.9 |

## Modules

No modules.

## Resources

| Name | Type |
|------|------|
| [aws_iam_policy.this](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/iam_policy) | resource |
| [aws_kms_alias.replica](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/kms_alias) | resource |
| [aws_kms_alias.this](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/kms_alias) | resource |
| [aws_kms_grant.replica](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/kms_grant) | resource |
| [aws_kms_grant.this](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/kms_grant) | resource |
| [aws_kms_key.this](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/kms_key) | resource |
| [aws_kms_key_policy.replica](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/kms_key_policy) | resource |
| [aws_kms_key_policy.this](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/kms_key_policy) | resource |
| [aws_kms_replica_key.replica](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/kms_replica_key) | resource |
| [aws_caller_identity.current](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/data-sources/caller_identity) | data source |
| [aws_iam_policy_document.external_kms](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/data-sources/iam_policy_document) | data source |
| [aws_iam_policy_document.internal_kms](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/data-sources/iam_policy_document) | data source |
| [aws_region.current](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/data-sources/region) | data source |
| [aws_region.replica](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/data-sources/region) | data source |

## Inputs

| Name | Description | Type | Default | Required |
|------|-------------|------|---------|:--------:|
| current\_account\_id | The account where this module is run. If not provided, will be fetched dynamically. If given, this value is trusted and will not be checked against the real current account ID. | `string` | `""` | no |
| current\_region | The region where this module is run. If not provided, will be fetched dynamically. If given, this value is trusted and will not be checked against the real current region. | `string` | `""` | no |
| iam\_policy\_create | Whether to generate external policies. For more complex use cases, this can be toggled off in favor of `var.iam_policy_export_json`. Only the required policies of the given scopes (keys of `var.iam_policy_entity_arns`) will be created. | `bool` | `false` | no |
| iam\_policy\_description | Description of the policies created when `var.iam_policy_create` is set. | `string` | `""` | no |
| iam\_policy\_entity\_arns | Restrict access the given IAM entities (roles or users).<br/>Allowed keys (scopes) are `ro`, `rw`, `rwd`, `audit` or `full`, each specifying the expected level of access for entities within them.<br/>Wildcards are allowed. | `map(map(string))` | `{}` | no |
| iam\_policy\_export\_actions | Whether to output IAM policies actions. A lightweight way to generate your own policies. | `bool` | `false` | no |
| iam\_policy\_export\_json | Whether to output KMS IAM policies as JSON strings. | `bool` | `false` | no |
| iam\_policy\_external\_actions\_audit | List of audit KMS actions that could be allowed for external IAM entities. This should be the bare minimum according to the intended key usage. | `list(string)` | <pre>[<br/>  "kms:DescribeKey",<br/>  "kms:GetKeyPolicy",<br/>  "kms:GetKeyRotationStatus",<br/>  "kms:GetPublicKey",<br/>  "kms:ListResourceTags"<br/>]</pre> | no |
| iam\_policy\_external\_actions\_delete | List of delete KMS actions that could be allowed for external IAM entities. | `list(string)` | <pre>[<br/>  "kms:DisableKey",<br/>  "kms:DisableKeyRotation",<br/>  "kms:Delete*"<br/>]</pre> | no |
| iam\_policy\_external\_actions\_full | List of admin KMS actions that could be allowed for external IAM entities. Most likely similar to `var.iam_policy_external_actions_rwd`. | `list(string)` | <pre>[<br/>  "kms:*"<br/>]</pre> | no |
| iam\_policy\_external\_actions\_ro | List of read-only KMS actions that could be allowed for external IAM entities. This should be the bare minimum according to the intended key usage. | `list(string)` | <pre>[<br/>  "kms:Decrypt"<br/>]</pre> | no |
| iam\_policy\_external\_actions\_rw | List of read-write KMS actions that could be allowed for external IAM entities. This should be the bare minimum according to the intended key usage. | `list(string)` | <pre>[<br/>  "kms:Decrypt",<br/>  "kms:GenerateDataKey"<br/>]</pre> | no |
| iam\_policy\_external\_actions\_rwd | List of read-write-delete KMS actions that could be allowed for external IAM entities. This should be the bare minimum according to the intended key usage. | `list(string)` | <pre>[<br/>  "kms:Decrypt",<br/>  "kms:GenerateDataKey"<br/>]</pre> | no |
| iam\_policy\_external\_resource\_arns | ARNs of KMS keys or aliases created outside this module to compose the external policies with. Along with KMS keys created inside this module, the given ARNs will be merged into external IAM policies created by this module. Keys are free values. | `map(string)` | `{}` | no |
| iam\_policy\_name\_template | Template for the name of the policies to attach to the given principals. The string expects a “%s” to be replaced by the scope of the policy (`ro`, `rw`, `rwd`, `audit` or `full` - keys of given `var.iam_policy_entity_arns`). | `string` | `""` | no |
| iam\_policy\_path | Path of the policies created when `var.iam_policy_create` is set. | `string` | `"/"` | no |
| iam\_policy\_restrict\_by\_account\_ids | Restrict resources created by this module by the given account IDs. Internal policies will whitelist given list. If `null`, no restriction will be applied. Especially useful when IAM entities contains accounts wildcards. | `list(string)` | `[]` | no |
| iam\_policy\_restrict\_by\_regions | Restrict resources created by this module by the given regions. Internal policies will whitelist given list. If `null`, no restriction will be applied. Especially useful when IAM entities contains region wildcards. | `list(string)` | `[]` | no |
| iam\_policy\_sid\_prefix | Use a prefix for all `Sid` of all the policies - internal and external - created by this module. | `string` | `""` | no |
| iam\_policy\_source\_arns | Restrict access to the key to the given ARN sources.<br/>Allowed keys (scopes) are `ro`, `rw`, `rwd`, `audit` or `full`, each specifying the expected level of access for entities within them.<br/>Wildcards are allowed. | `map(map(string))` | `{}` | no |
| iam\_policy\_tags | Tags of the policies created when `var.iam_policy_create` is set. Will be merge with `var.tags`. | `map(string)` | `{}` | no |
| kms\_grants | Grants to create.<br/>Keys must correspond to `var.kms_keys` keys.<br/>Second level keys are free values.<br/><br/>  - name                  (required, string):       A friendly name for identifying the grant.<br/>  - grantee\_principal     (required, string):       The principal that is given permission to perform the operations that the grant permits in ARN format. Note that due to eventual consistency issues around IAM principals, terraform's state may not always be refreshed to reflect what is true in AWS.<br/>  - operations            (required, list(string)): A list of operations that the grant permits. The permitted values are: `Decrypt`, `Encrypt`, `GenerateDataKey`, `GenerateDataKeyWithoutPlaintext`, `ReEncryptFrom`, `ReEncryptTo`, `Sign`, `Verify`, `GetPublicKey`, `CreateGrant`, `RetireGrant`, `DescribeKey`, `GenerateDataKeyPair`, or `GenerateDataKeyPairWithoutPlaintext`.<br/>  - retiring\_principal    (optional, string):       The principal that is given permission to retire the grant by using RetireGrant operation in ARN format. Note that due to eventual consistency issues around IAM principals, terraform's state may not always be refreshed to reflect what is true in AWS.<br/>  - grant\_creation\_tokens (optional, list(string)): A list of grant tokens to be used when creating the grant. See Grant Tokens for more information about grant tokens.<br/>  - retire\_on\_delete      (optional, bool):         If set to false (the default) the grants will be revoked upon deletion, and if set to true the grants will try to be retired upon deletion. Note that retiring grants requires special permissions, hence why we default to revoking grants. See RetireGrant for more information. | <pre>map(map(object({<br/>    name                  = string<br/>    grantee_principal     = string<br/>    operations            = list(string)<br/>    retiring_principal    = optional(string)<br/>    grant_creation_tokens = optional(list(string))<br/>    retire_on_delete      = optional(bool, false)<br/>  })))</pre> | `{}` | no |
| kms\_keys | Options of the KMS key to create.<br/>If given and and `var.replica_enabled`, this key will be use also for the replica, unless `var.replica_kms_key_id` is set.<br/>If given, `var.kms_key_id` will be ignored.<br/>Keys are free values.<br/><br/>  - alias                   (required, string):                      KMS key alias; Display name of the KMS key. Omit the `alias/`.<br/>  - principal\_actions       (optional, map(list(string))):           List of read-only, read-write, read-write-delete, audit and full allowed IAM actions for the principals defined by "var.iam\_policy\_entity\_arns[key]" variable, or nobody, if variable value is undefined. The four expected keys are "ro", "rw", "rwd", "delete", "audit" and "full". If this is not provided, actions default to corresponding `var.iam_policy_external_actions_...`.<br/>  - policy\_json             (optional, string):                      Additional policy to attach to the KMS key, merged with a baseline internal policy. Make sure to provide "var.iam\_admin\_arns" to avoid getting blocked.<br/>  - service\_principal       (optional, string):                      The name of the service authorized to access the key.<br/>  - service\_actions         (optional, list(string)):                List of allowed IAM actions for the "service\_principal" authorized to access the key.<br/>  - description             (optional, string, ""):                  Description of the key.<br/>  - usage                   (optional, string, "ENCRYPT\_DECRYPT"):   Specifies the intended use of the key. Valid values: `ENCRYPT_DECRYPT`, `SIGN_VERIFY`, or `GENERATE_VERIFY_MAC`.<br/>  - spec                    (optional, string, "SYMMETRIC\_DEFAULT"): Specifies whether the key contains a symmetric key or an asymmetric key pair and the encryption algorithms or signing algorithms that the key supports. Valid values: `SYMMETRIC_DEFAULT`, `RSA_2048`, `RSA_3072`, `RSA_4096`, `HMAC_256`, `ECC_NIST_P256`, `ECC_NIST_P384`, `ECC_NIST_P521`, or `ECC_SECG_P256K1`.<br/>  - rotation\_enabled        (optional, bool, true):                  Whether to automatically rotate the KMS key linked to the secrets.<br/>  - deletion\_window\_in\_days (optional, number, 7):                   The waiting period, specified in number of days. After the waiting period ends, AWS KMS deletes the KMS key. If you specify a value, it must be between `7` and `30`, inclusive. If you do not specify a value, it defaults to `7`.<br/>  - tags                    (optional, map(string)):                 Tags to be used by the KMS key of this module. | <pre>map(object({<br/>    alias                   = string<br/>    principal_actions       = optional(map(list(string)))<br/>    policy_json             = optional(string, null)<br/>    service_principal       = optional(string, null)<br/>    service_actions         = optional(list(string), null)<br/>    description             = optional(string, "")<br/>    usage                   = optional(string, "ENCRYPT_DECRYPT")<br/>    spec                    = optional(string, "SYMMETRIC_DEFAULT")<br/>    rotation_enabled        = optional(bool, true)<br/>    deletion_window_in_days = optional(number, 7)<br/>    tags                    = optional(map(string), {})<br/>  }))</pre> | `{}` | no |
| prefix | Prefix to be used for all resources names. Specifically useful for tests. A 4-characters alphanumeric string. It must start by a lowercase letter. | `string` | `""` | no |
| replica\_enabled | Whether to replicate the KMS keys into the `aws.replica` region. | `bool` | `false` | no |
| replica\_region | The region for the KMS replica. If not provided, will be fetched dynamically. If given, this value is trusted and will not be checked against the real replica region. | `string` | `""` | no |
| tags | Tags to be shared with all resources of this module. | `map(string)` | `{}` | no |

## Outputs

| Name | Description |
|------|-------------|
| aws\_iam\_policies | n/a |
| aws\_kms\_aliases | n/a |
| aws\_kms\_grants | n/a |
| aws\_kms\_keys | n/a |
| precomputed | n/a |
| replica\_aws\_kms\_aliases | n/a |
| replica\_aws\_kms\_grants | n/a |
| replica\_aws\_kms\_keys | n/a |
<!-- END_TF_DOCS -->

## Versioning

This repository follows [Semantic Versioning 2.0.0](https://semver.org/)

## Git Hooks

This repository uses [pre-commit](https://pre-commit.com/) hooks.
