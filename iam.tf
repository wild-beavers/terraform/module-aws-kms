####
# Variables
####

variable "iam_policy_export_json" {
  description = "Whether to output KMS IAM policies as JSON strings."
  type        = bool
  default     = false
  nullable    = false
}

variable "iam_policy_export_actions" {
  description = "Whether to output IAM policies actions. A lightweight way to generate your own policies."
  type        = bool
  default     = false
  nullable    = false
}

variable "iam_policy_external_actions_ro" {
  description = "List of read-only KMS actions that could be allowed for external IAM entities. This should be the bare minimum according to the intended key usage."
  type        = list(string)
  default = [
    "kms:Decrypt",
  ]
  nullable = false

  validation {
    condition     = length(var.iam_policy_external_actions_ro) > 0
    error_message = "“var.iam_policy_external_actions_ro” is invalid: it cannot be empty."
  }
}

variable "iam_policy_external_actions_rw" {
  description = "List of read-write KMS actions that could be allowed for external IAM entities. This should be the bare minimum according to the intended key usage."
  type        = list(string)
  default = [
    "kms:Decrypt",
    "kms:GenerateDataKey",
  ]
  nullable = false

  validation {
    condition     = length(var.iam_policy_external_actions_rw) > 0
    error_message = "“var.iam_policy_external_actions_rw” is invalid: it cannot be empty."
  }
}

variable "iam_policy_external_actions_rwd" {
  description = "List of read-write-delete KMS actions that could be allowed for external IAM entities. This should be the bare minimum according to the intended key usage."
  type        = list(string)
  default = [
    "kms:Decrypt",
    "kms:GenerateDataKey",
  ]
  nullable = false

  validation {
    condition     = length(var.iam_policy_external_actions_rwd) > 0
    error_message = "“var.iam_policy_external_actions_rwd” is invalid: it cannot be empty."
  }
}

variable "iam_policy_external_actions_delete" {
  description = "List of delete KMS actions that could be allowed for external IAM entities."
  type        = list(string)
  default = [
    "kms:DisableKey",
    "kms:DisableKeyRotation",
    "kms:Delete*",
  ]
  nullable = false

  validation {
    condition     = length(var.iam_policy_external_actions_delete) > 0
    error_message = "“var.iam_policy_external_actions_delete” is invalid: it cannot be empty."
  }
}

variable "iam_policy_external_actions_audit" {
  description = "List of audit KMS actions that could be allowed for external IAM entities. This should be the bare minimum according to the intended key usage."
  type        = list(string)
  default = [
    "kms:DescribeKey",
    "kms:GetKeyPolicy",
    "kms:GetKeyRotationStatus",
    "kms:GetPublicKey",
    "kms:ListResourceTags",
  ]
  nullable = false

  validation {
    condition     = length(var.iam_policy_external_actions_audit) > 0
    error_message = "“var.iam_policy_external_actions_audit” is invalid: it cannot be empty."
  }
}

variable "iam_policy_external_actions_full" {
  description = "List of admin KMS actions that could be allowed for external IAM entities. Most likely similar to `var.iam_policy_external_actions_rwd`."
  type        = list(string)
  default     = ["kms:*"]
  nullable    = false

  validation {
    condition     = length(var.iam_policy_external_actions_full) > 0
    error_message = "“var.iam_policy_external_actions_full” is invalid: it cannot be empty."
  }
}

variable "iam_policy_external_resource_arns" {
  description = "ARNs of KMS keys or aliases created outside this module to compose the external policies with. Along with KMS keys created inside this module, the given ARNs will be merged into external IAM policies created by this module. Keys are free values."
  type        = map(string)
  default     = {}
  nullable    = false

  validation {
    condition = ((!contains([
      for arn in var.iam_policy_external_resource_arns :
      (arn == null || can(regex("^arn:aws(-us-gov|-cn)?:kms:([a-z]{2}-[a-z]{4,10}-[1-9]{1})?:([0-9]{12}|aws):(key|alias)/[0-9A-Za-z\\/_-]{2,256}$", arn)))
    ], false)))
    error_message = "One or more “var.iam_policy_external_resource_arns” are invalid."
  }
}

variable "iam_policy_create" {
  description = "Whether to generate external policies. For more complex use cases, this can be toggled off in favor of `var.iam_policy_export_json`. Only the required policies of the given scopes (keys of `var.iam_policy_entity_arns`) will be created."
  type        = bool
  default     = false
  nullable    = false
}

variable "iam_policy_name_template" {
  type        = string
  description = "Template for the name of the policies to attach to the given principals. The string expects a “%s” to be replaced by the scope of the policy (`ro`, `rw`, `rwd`, `audit` or `full` - keys of given `var.iam_policy_entity_arns`)."
  default     = ""
  nullable    = false

  validation {
    condition     = !local.iam_should_create_external_policy || (can(regex("^[a-zA-Z0-9+=,\\.\\%/@-]+$", var.iam_policy_name_template)) && can(regex("\\%s", var.iam_policy_name_template)))
    error_message = "“var.iam_policy_name_template” is invalid."
  }
}

variable "iam_policy_path" {
  description = "Path of the policies created when `var.iam_policy_create` is set."
  type        = string
  default     = "/"
  nullable    = false

  validation {
    condition     = var.iam_policy_path == "/" || can(regex("^/[a-z0-9/]+/$", var.iam_policy_path))
    error_message = "The var.iam_policy_path must match “^/[a-z0-9/]+/$” (or be /)."
  }
}

variable "iam_policy_description" {
  type        = string
  description = "Description of the policies created when `var.iam_policy_create` is set."
  default     = ""
  nullable    = false

  validation {
    condition     = !local.iam_should_create_external_policy || (1 <= length(var.iam_policy_description) && length(var.iam_policy_description) <= 1024)
    error_message = "“var.iam_policy_description” is invalid."
  }
}

variable "iam_policy_tags" {
  description = "Tags of the policies created when `var.iam_policy_create` is set. Will be merge with `var.tags`."
  type        = map(string)
  default     = {}
  nullable    = false
}

####
# Locals
####

locals {
  iam_export_json                     = var.iam_policy_export_json && local.kms_should_create
  iam_should_create_external_policy   = var.iam_policy_create && local.kms_should_create
  iam_should_create_external_document = local.iam_should_create_external_policy || local.iam_export_json
  iam_external_actions = {
    ro     = var.iam_policy_external_actions_ro
    rw     = var.iam_policy_external_actions_rw
    rwd    = var.iam_policy_external_actions_rwd
    delete = var.iam_policy_external_actions_delete
    audit  = var.iam_policy_external_actions_audit
    full   = var.iam_policy_external_actions_full
  }
}

####
# Data
####

data "aws_iam_policy_document" "external_kms" {
  for_each = local.iam_should_create_external_document ? local.iam_external_actions : {}

  statement {
    sid     = "${try(chomp(var.iam_policy_sid_prefix), "")}KMS${upper(each.key)}"
    effect  = "Allow"
    actions = each.value
    resources = compact(concat(
      flatten([for key, kms_key in var.kms_keys : [
        provider::aws::arn_build("aws", "kms", local.current_region, local.current_account_id, "alias/${var.prefix}${kms_key.alias}"),
        var.replica_enabled ? provider::aws::arn_build("aws", "kms", local.replica_region, local.current_account_id, "alias/${var.prefix}${kms_key.alias}") : null,
      ]]),
      [
        for arn in var.iam_policy_external_resource_arns :
        arn
      ]
    ))
  }
}

####
# Resources
####

resource "aws_iam_policy" "this" {
  for_each = local.iam_should_create_external_policy ? var.iam_policy_entity_arns : {}

  name   = format("${var.prefix}${var.iam_policy_name_template}", each.key)
  path   = var.iam_policy_path
  policy = data.aws_iam_policy_document.external_kms[each.key].json

  description = var.iam_policy_description

  tags = merge(
    {
      Name        = format("${var.prefix}${var.iam_policy_name_template}", each.key)
      Description = var.iam_policy_description
    },
    local.tags,
    var.iam_policy_tags,
  )
}

####
# Outputs
####

output "aws_iam_policies" {
  value = local.iam_should_create_external_policy ? { for k, v in aws_iam_policy.this :
    k => { for i, j in v : i => j if !contains(["attachment_count", "name_prefix", "policy", "tags_all", "tags"], i) }
  } : null
}
