#####
# Default
#####

data "aws_kms_key" "default_kms" {
  for_each = module.default.aws_kms_keys

  key_id = each.value.id

  depends_on = [
    module.default
  ]
}

check "default" {
  assert {
    condition = (
      module.default.aws_kms_keys.one.multi_region == true &&
      module.default.replica_aws_kms_keys.one.arn == data.aws_kms_key.default_kms["one"].multi_region_configuration[0].replica_keys[0].arn &&
      module.default.aws_kms_keys.two.multi_region == true &&
      module.default.replica_aws_kms_keys.two.arn == data.aws_kms_key.default_kms["two"].multi_region_configuration[0].replica_keys[0].arn
    )
    error_message = "KMS functional test fails: replica error"
  }

  assert {
    condition = (
      module.default.aws_iam_policies == null
    )
    error_message = "KMS functional test fails: external policy error"
  }

  assert {
    condition = (
      module.default.precomputed.aws_iam_policy_actions == null
    )
    error_message = "KMS functional test fails: exported actions"
  }

  assert {
    condition = (
      contains(module.default.precomputed.aws_kms_aliases.one.arns, module.default.aws_kms_aliases.one.arn) &&
      contains(module.default.precomputed.aws_kms_aliases.one.arns, module.default.replica_aws_kms_aliases.one.arn) &&
      contains(module.default.precomputed.aws_kms_aliases.two.arns, module.default.aws_kms_aliases.two.arn) &&
      contains(module.default.precomputed.aws_kms_aliases.two.arns, module.default.replica_aws_kms_aliases.two.arn)
    )
    error_message = "KMS functional test fails: precomputed outputs"
  }
}

#####
# IAM
#####

check "iam" {
  assert {
    condition = (
      module.iam.aws_kms_keys.iam.multi_region == false &&
      module.iam.replica_aws_kms_keys == null
    )
    error_message = "KMS functional test fails: replica error"
  }

  assert {
    condition = (
      contains(jsondecode(module.iam.precomputed.aws_iam_policies.ro.json)["Statement"][0]["Resource"], aws_kms_key.example.arn) &&
      contains(jsondecode(module.iam.precomputed.aws_iam_policies.ro.json)["Statement"][0]["Resource"], module.iam.aws_kms_aliases.iam.arn) &&
      jsondecode(module.iam.precomputed.aws_iam_policies.ro.json)["Statement"][0]["Action"] == "kms:Decrypt" &&
      contains(jsondecode(module.iam.precomputed.aws_iam_policies.rw.json)["Statement"][0]["Resource"], aws_kms_key.example.arn) &&
      contains(jsondecode(module.iam.precomputed.aws_iam_policies.rw.json)["Statement"][0]["Resource"], module.iam.aws_kms_aliases.iam.arn) &&
      length(jsondecode(module.iam.precomputed.aws_iam_policies.rw.json)["Statement"][0]["Action"]) == 2 &&
      contains(jsondecode(module.iam.precomputed.aws_iam_policies.rw.json)["Statement"][0]["Action"], "kms:GenerateDataKey") &&
      contains(jsondecode(module.iam.precomputed.aws_iam_policies.rw.json)["Statement"][0]["Action"], "kms:Decrypt") &&
      contains(jsondecode(module.iam.precomputed.aws_iam_policies.rw.json)["Statement"][0]["Action"], "kms:GenerateDataKey")
    )
    error_message = "KMS functional test fails: external policy document error"
  }

  assert {
    condition = (
      length(module.iam.aws_iam_policies) == 3 &&
      module.iam.aws_iam_policies.ro.name == "${local.prefix}kmspolicy-ro" &&
      module.iam.aws_iam_policies.rw.name == "${local.prefix}kmspolicy-rw" &&
      module.iam.aws_iam_policies.full.name == "${local.prefix}kmspolicy-full"
    )
    error_message = "KMS functional test fails: external policy error"
  }

  assert {
    condition = (
      length(module.iam.precomputed.aws_iam_policy_actions) == 5
    )
    error_message = "KMS functional test fails: exported actions"
  }

  assert {
    condition = (
      module.iam.aws_iam_policies.full.arn == module.iam.precomputed.aws_iam_policies.full.arn &&
      module.iam.aws_iam_policies.ro.arn == module.iam.precomputed.aws_iam_policies.ro.arn &&
      module.iam.aws_iam_policies.rw.arn == module.iam.precomputed.aws_iam_policies.rw.arn &&
      contains(module.iam.precomputed.aws_kms_aliases.iam.arns, module.iam.aws_kms_aliases.iam.arn)
    )
    error_message = "KMS functional test fails: precomputed outputs"
  }
}

#####
# Minimal
#####

check "minimal" {
  assert {
    condition = (
      module.minimal.aws_kms_keys.minimal.multi_region == false &&
      module.minimal.replica_aws_kms_keys == null
    )
    error_message = "KMS functional test fails: replica error"
  }

  assert {
    condition = (
      module.minimal.precomputed.aws_iam_policies == null &&
      module.minimal.precomputed.aws_iam_policy_actions == null
    )
    error_message = "KMS functional test fails: external policy error"
  }

  assert {
    condition = (
      contains(module.minimal.precomputed.aws_kms_aliases.minimal.arns, module.minimal.aws_kms_aliases.minimal.arn)
    )
    error_message = "KMS functional test fails: precomputed outputs"
  }
}
