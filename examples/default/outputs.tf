output "default" {
  value = module.default
}

output "iam" {
  value = module.iam
}

output "minimal" {
  value = module.minimal
}
