data "aws_caller_identity" "current" {}

data "aws_region" "current" {}

data "aws_region" "replica" {
  provider = aws.replica
}

locals {
  prefix     = "${random_string.start_letter.result}${random_string.this.result}"
  caller_arn = replace(replace(join("/", chunklist(split("/", data.aws_caller_identity.current.arn), 2)[0]), "sts", "iam"), "assumed-role", "role")
}

resource "random_string" "start_letter" {
  length  = 1
  upper   = false
  special = false
  numeric = false
}

resource "random_string" "this" {
  length  = 3
  upper   = false
  special = false
}

resource "aws_iam_group" "test" {
  name = "${local.prefix}-test"
}

resource "aws_iam_user" "test" {
  name = "${local.prefix}-test"
}

resource "aws_iam_user" "test2" {
  name = "${local.prefix}-test2"
}

resource "aws_iam_role" "test" {
  name = "${local.prefix}-test"
  assume_role_policy = jsonencode({
    Version = "2012-10-17"
    Statement = [
      {
        Action    = "sts:AssumeRole"
        Effect    = "Allow"
        Sid       = ""
        Principal = { AWS = local.caller_arn }
      },
    ]
  })
}

resource "aws_kms_key" "example" {
  description = "example key for tests"
}

#####
# Default
#####

data "aws_iam_policy_document" "additional" {
  statement {
    sid = "Additional"
    actions = [
      "kms:DescribeKey",
    ]
    principals {
      type        = "AWS"
      identifiers = ["*"]
    }
    resources = ["*"]
    condition {
      test     = "StringLike"
      values   = ["arn:aws:iam::${data.aws_caller_identity.current.account_id}:role/AdminRole"]
      variable = "aws:principalArn"
    }
  }
}

module "default" {
  source = "../../"

  prefix             = local.prefix
  current_region     = data.aws_region.current.name
  replica_region     = data.aws_region.replica.name
  current_account_id = data.aws_caller_identity.current.account_id

  kms_keys = {
    one = {
      alias       = "example-one"
      policy_json = data.aws_iam_policy_document.additional.json
      principal_actions = {
        ro = [
          "kms:Decrypt",
          "kms:GenerateDataKey",
        ]
        rw = [
          "kms:CreateGrant",
          "kms:DescribeKey",
          "kms:GenerateDataKey",
          "kms:Decrypt",
        ]
        rwd = [
          "kms:CreateGrant",
          "kms:DescribeKey",
          "kms:RetireGrant",
          "kms:GenerateDataKey",
          "kms:Decrypt",
        ]
      }
      service_principal = "secretsmanager"
      service_actions = [
        "kms:CreateGrant",
        "kms:GenerateDataKey",
        "kms:Decrypt",
      ]
      description             = "An example of KMS key for S3"
      deletion_window_in_days = 7
    }

    two = {
      alias = "example-two"
      principal_actions = {
        ro = [
          "kms:Decrypt",
          "kms:DescribeKey",
        ]
        rw = [
          "kms:DescribeKey",
          "kms:Decrypt",
          "kms:Encrypt",
        ]
        rwd = [
          "kms:DescribeKey",
          "kms:Decrypt",
          "kms:Encrypt",
        ]
      }
      service_principal = "s3"
      service_actions   = ["kms:Decrypt"]
      description       = "An example of KMS key for Secrets Manager"
      rotation_enabled  = false
      tags = {
        additional = "value"
      }
      deletion_window_in_days = 7
    }
  }
  replica_enabled = true
  iam_policy_entity_arns = {
    ro    = { 0 = aws_iam_user.test.arn },
    rw    = { 0 = aws_iam_user.test2.arn, 1 = aws_iam_role.test.arn }
    rwd   = { wildcard_region = "arn:aws:sts:*:${data.aws_caller_identity.current.account_id}:role/bar" }
    audit = { 0 = "arn:aws:iam::${data.aws_caller_identity.current.account_id}:role/AdminRole" }
    full  = { 0 = local.caller_arn }
  }
  iam_policy_source_arns = {
    ro = { 0 = "arn:aws:cloudfront::${data.aws_caller_identity.current.account_id}:distribution/XXXXBUGSBYXXXX" }
    rw = { wildcard_region = "arn:aws:foo:*:${data.aws_caller_identity.current.account_id}:foo/foo" }
  }
  iam_policy_sid_prefix              = local.prefix
  iam_policy_restrict_by_account_ids = [data.aws_caller_identity.current.account_id]
  iam_policy_restrict_by_regions     = [data.aws_region.current.name]

  tags = {}

  kms_grants = {
    one = {
      tftest = {
        name              = "iamtftest2"
        grantee_principal = aws_iam_role.test.arn
        operations        = ["Decrypt"]
      }
    }
  }

  providers = {
    aws.replica = aws.replica
  }
}

#####
# IAM
#####

module "iam" {
  source = "../../"

  prefix             = local.prefix
  current_region     = data.aws_region.current.name
  replica_region     = data.aws_region.replica.name
  current_account_id = data.aws_caller_identity.current.account_id

  kms_keys = {
    iam = {
      alias = "example-iam"
      principal_actions = {
        ro = [
          "kms:Decrypt",
        ]
        rw = [
          "kms:GenerateDataKey",
          "kms:Decrypt",
        ]
        rwd = [
          "kms:GenerateDataKey",
          "kms:Decrypt",
        ]
      }
      service_principal = "ecr"
      service_actions = [
        "kms:CreateGrant",
        "kms:GenerateDataKey",
        "kms:Decrypt",
      ]
      description             = "An example of KMS key for no ECR"
      deletion_window_in_days = 7
    }
  }

  iam_policy_entity_arns = {
    ro   = { 0 = aws_iam_user.test.arn },
    rw   = { 0 = aws_iam_user.test2.arn, 1 = aws_iam_role.test.arn }
    full = { 0 = local.caller_arn, 1 = null }
  }
  iam_policy_sid_prefix = local.prefix

  iam_policy_export_json = true
  iam_policy_external_actions_ro = [
    "kms:Decrypt",
  ]
  iam_policy_external_actions_rw = [
    "kms:Decrypt",
    "kms:GenerateDataKey",
  ]
  iam_policy_export_actions = true
  iam_policy_create         = true
  iam_policy_name_template  = "kmspolicy-%s"
  iam_policy_description    = "A test policy to access ECR KMS"
  iam_policy_external_resource_arns = {
    0 = aws_kms_key.example.arn
  }

  kms_grants = {
    iam = {
      tftest = {
        name              = "iamtftest"
        grantee_principal = aws_iam_role.test.arn
        operations        = ["Encrypt", "Decrypt", "GenerateDataKey", "DescribeKey", "CreateGrant", "GenerateDataKeyWithoutPlaintext", "ReEncryptFrom", "ReEncryptTo", "RetireGrant"]
      }
    }
  }

  providers = {
    aws.replica = aws.replica
  }
}

#####
# Minimal
#####

module "minimal" {
  source = "../../"

  prefix = local.prefix
  kms_keys = {
    minimal = {
      alias                   = "example-minimal"
      description             = "An example of KMS key for no service in particular"
      deletion_window_in_days = 7
    }
  }

  replica_enabled = false

  iam_policy_entity_arns = {
    full = { 0 = local.caller_arn }
  }

  providers = {
    aws.replica = aws.replica
  }
}
