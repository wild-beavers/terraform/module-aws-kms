variable "current_region" {
  description = "The region where this module is run. If not provided, will be fetched dynamically. If given, this value is trusted and will not be checked against the real current region."
  default     = ""
  type        = string
  nullable    = false

  validation {
    condition     = can(regex("^([a-z]{2}-[a-z]{4,10}-[1-9]{1})?$", var.current_region))
    error_message = "“var.current_region” does not match '^([a-z]{2}-[a-z]{4,10}-[1-9]{1})?$'."
  }
}

variable "replica_region" {
  description = "The region for the KMS replica. If not provided, will be fetched dynamically. If given, this value is trusted and will not be checked against the real replica region."
  default     = ""
  type        = string
  nullable    = false

  validation {
    condition     = var.replica_region == "" || var.replica_region != var.current_region
    error_message = "“var.replica_region” cannot be the same as ${var.current_region}."
  }
  validation {
    condition     = can(regex("^([a-z]{2}-[a-z]{4,10}-[1-9]{1})?$", var.replica_region))
    error_message = "“var.replica_region” does not match '^([a-z]{2}-[a-z]{4,10}-[1-9]{1})?$'."
  }
}

variable "current_account_id" {
  description = "The account where this module is run. If not provided, will be fetched dynamically. If given, this value is trusted and will not be checked against the real current account ID."
  default     = ""
  type        = string
  nullable    = false

  validation {
    condition     = can(regex("^([0-9]{12})?$", var.current_account_id))
    error_message = "“var.current_account_id” does not match '^[0-9]{12}$'."
  }
}
